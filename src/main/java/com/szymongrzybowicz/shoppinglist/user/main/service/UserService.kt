package com.szymongrzybowicz.shoppinglist.user.main.service

import com.szymongrzybowicz.shoppinglist.base.EmptyResponse
import com.szymongrzybowicz.shoppinglist.base.ErrorCodes
import com.szymongrzybowicz.shoppinglist.user.main.controller.register.RegisterResponse
import com.szymongrzybowicz.shoppinglist.user.main.controller.search.SearchResponse
import com.szymongrzybowicz.shoppinglist.user.main.service.entity.BlockedUser
import com.szymongrzybowicz.shoppinglist.user.main.service.entity.User
import com.szymongrzybowicz.shoppinglist.user.main.service.mapper.UserMapper
import com.szymongrzybowicz.shoppinglist.user.main.service.repository.UserRepository
import com.szymongrzybowicz.shoppinglist.user.main.service.security.UserContext
import com.szymongrzybowicz.shoppinglist.user.main.service.security.UserRole
import org.hibernate.search.engine.search.query.SearchResult
import org.hibernate.search.mapper.orm.Search
import org.slf4j.LoggerFactory
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.sql.Timestamp
import javax.persistence.EntityManager

@Service
@Transactional
class UserService(
    private var passwordEncoder: PasswordEncoder,
    private val repository: UserRepository,
    private val userContext: UserContext,
    private val entityManager: EntityManager,
    private val userMapper: UserMapper
) {

    fun createUser(
        firstName: String,
        lastName: String,
        password: String?,
        mail: String
    ): RegisterResponse {
        logger.info("createUser()")
        repository.findByMail(mail)?.let {
            val message = listOf("User with that email already exists")
            return RegisterResponse(true, message, ErrorCodes.USER_ALREADY_EXISTS)
        }
        val user = User(
            active = false,
            role = UserRole.ROLE_USER,
            firstName = firstName,
            lastName = lastName,
            password = passwordEncoder.encode(password),
            mail = mail
        )
        repository.save(user)
        //todo send activation email
        return RegisterResponse()
    }

    fun block(userId: Long, block: Boolean): EmptyResponse {
        if (block) {
            val currentUserId = userContext.getCurrentUser().id
            if (!repository.isUserBlockedByUser(userId, currentUserId)) {
                val blockedUser = BlockedUser(
                    userFrom = repository.findById(currentUserId).get(),
                    userTo = repository.findById(userId).get(),
                    createDate = Timestamp(System.currentTimeMillis())
                )
                entityManager.persist(blockedUser)
            }
        } else {
            repository.removeBlockade(userContext.getCurrentUser().id, userId)
        }
        return EmptyResponse(false, null, null)
    }

    fun search(text: String): SearchResponse {
        val searchSession = Search.session(entityManager)

        val searchResult = searchSession.search(User::class.java)
            .where { f ->
                f.match()
                    .fields("firstName", "lastName", "mail")
                    .matching(text)
            }
            .fetch(50) as SearchResult<User>

        val result = searchResult.hits().map { e -> userMapper.mapToDto(e) }
        return SearchResponse(data = SearchResponse.Data(result))
    }

    private val logger = LoggerFactory.getLogger(javaClass)
}