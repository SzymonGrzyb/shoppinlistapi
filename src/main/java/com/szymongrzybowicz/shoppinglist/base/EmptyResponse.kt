package com.szymongrzybowicz.shoppinglist.base

class EmptyResponse(
    hasErrors: Boolean = false,
    message: List<String>? = null,
    errorCode: Int? = null
) : Response(hasErrors, message, errorCode)