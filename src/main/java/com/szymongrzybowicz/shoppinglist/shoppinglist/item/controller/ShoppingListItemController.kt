package com.szymongrzybowicz.shoppinglist.shoppinglist.item.controller

import com.szymongrzybowicz.shoppinglist.base.EmptyResponse
import com.szymongrzybowicz.shoppinglist.shoppinglist.item.controller.create.CreateRequest
import com.szymongrzybowicz.shoppinglist.shoppinglist.item.controller.create.CreateResponse
import com.szymongrzybowicz.shoppinglist.shoppinglist.item.controller.get.shopping.list.items.GetShoppingListItemsResponse
import com.szymongrzybowicz.shoppinglist.shoppinglist.item.service.ShoppingListItemService
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import java.math.BigDecimal

@RestController
@RequestMapping(
    "/shoppingList/item",
    produces = [MediaType.APPLICATION_JSON_VALUE],
    consumes = [MediaType.APPLICATION_JSON_VALUE]
)
@CrossOrigin("*")
class ShoppingListItemController(private val service: ShoppingListItemService) {

    @GetMapping
    fun getItems(
        @RequestParam(required = false) page: Int?,
        @RequestParam(required = true) idShoppingList: Long
    ): GetShoppingListItemsResponse {
        return service.getItems(page, idShoppingList)
    }

    @PostMapping("/create")
    fun createItem(@RequestBody request: CreateRequest): CreateResponse {
        return service.create(request)
    }

    @PatchMapping("/update")
    fun updateItem(
        @RequestParam(required = true) id: Long,
        @RequestParam(required = false) name: String?,
        @RequestParam(required = false) quantity: BigDecimal?,
        @RequestParam(required = false) unit: String?,
        @RequestParam(required = false) bought: Boolean?
    ): EmptyResponse {
        return service.updateItem(id, name, quantity, unit, bought)
    }

    @DeleteMapping
    fun deleteItem(
        @RequestParam(required = true) id: Long,
    ): EmptyResponse {
        return service.delete(id)
    }


    @PostMapping("/markAsBought")
    fun markAsBought(
        @RequestParam(required = true) id: Long,
        @RequestParam(required = true) bought: Boolean
    ): EmptyResponse {
        return service.updateItem(id, null, null, null, bought)
    }
}