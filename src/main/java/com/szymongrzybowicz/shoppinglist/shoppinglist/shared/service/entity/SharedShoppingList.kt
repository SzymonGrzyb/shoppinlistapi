package com.szymongrzybowicz.shoppinglist.shoppinglist.shared.service.entity

import com.szymongrzybowicz.shoppinglist.shoppinglist.main.service.entity.ShoppingList
import com.szymongrzybowicz.shoppinglist.shoppinglist.permission.ShoppingListPermission
import com.szymongrzybowicz.shoppinglist.user.main.service.entity.User
import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(indexes = [Index(columnList = "user_id, shopping_list_id", unique = true)])
class SharedShoppingList(

    @Id
    @Column
    @GeneratedValue
    val id: Long = -1,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    val shoppingList: ShoppingList,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    val user: User,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    var shoppingListPermission: ShoppingListPermission,

    @Column
    val createDate: Timestamp,

    @Column
    val modificationDate: Timestamp

)