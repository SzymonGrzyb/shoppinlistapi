package com.szymongrzybowicz.shoppinglist.user.main.service.security

object UserRole {
    const val ROLE_USER = "ROLE_USER"
    const val ROLE_ADMIN = "ROLE_ADMIN"
}