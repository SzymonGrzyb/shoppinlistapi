package com.szymongrzybowicz.shoppinglist.user.main.service.security

import com.szymongrzybowicz.shoppinglist.user.main.service.repository.UserRepository
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service("userDetailsService")
open class MyUserDetailsService(
    private val userRepository: UserRepository
) : UserDetailsService {

    @Transactional(readOnly = true)
    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        userRepository.findByMail(username)?.let { user ->
            return UserDetails(user)
        }
        throw UsernameNotFoundException("User not found.")
    }
}