package com.szymongrzybowicz.shoppinglist.shoppinglist.item.dto

import java.math.BigDecimal
import java.sql.Timestamp

class ShoppingListItemDto(
    val id: Long = -1,
    val name: String,
    val quantity: BigDecimal,
    val unit: String,
    val bought: Boolean,
    val createDate: Timestamp,
    val modificationDate: Timestamp
)