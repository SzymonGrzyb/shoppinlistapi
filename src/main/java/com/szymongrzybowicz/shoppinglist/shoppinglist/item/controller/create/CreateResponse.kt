package com.szymongrzybowicz.shoppinglist.shoppinglist.item.controller.create

import com.szymongrzybowicz.shoppinglist.base.Response

class CreateResponse(
    hasErrors: Boolean = false,
    message: List<String>? = null,
    errorCode: Int? = null,
    data: Data? = null
) : Response(
    hasErrors,
    message,
    errorCode
) {
    class Data(
        val id: Long,
        val name: String
    )
}