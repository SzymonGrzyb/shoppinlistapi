package com.szymongrzybowicz.shoppinglist.shoppinglist.main.service.mapper

import com.szymongrzybowicz.shoppinglist.shoppinglist.main.dto.ShoppingListDto
import com.szymongrzybowicz.shoppinglist.shoppinglist.main.service.entity.ShoppingList
import com.szymongrzybowicz.shoppinglist.user.main.service.mapper.UserMapper
import com.szymongrzybowicz.shoppinglist.user.main.service.security.UserContext
import org.springframework.stereotype.Component

@Component
class ShoppingListMapper(private val userMapper: UserMapper, private val userContext: UserContext) {

    fun map(shoppingList: ShoppingList): ShoppingListDto {
        val currentUserId = userContext.getCurrentUser().id
        val userDto = if (shoppingList.owner.id == currentUserId) null else userMapper.mapToDto(shoppingList.owner)
        return ShoppingListDto(
            shoppingList.id,
            userDto,
            shoppingList.name,
            shoppingList.createDate,
            shoppingList.modificationDate,
            shoppingList.boughtItemsCount,
            shoppingList.notBoughtItemsCount
        )
    }

}