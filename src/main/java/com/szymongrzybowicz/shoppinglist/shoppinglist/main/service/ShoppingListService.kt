package com.szymongrzybowicz.shoppinglist.shoppinglist.main.service

import com.szymongrzybowicz.shoppinglist.base.EmptyResponse
import com.szymongrzybowicz.shoppinglist.base.ErrorCodes
import com.szymongrzybowicz.shoppinglist.shoppinglist.main.controller.create.CreateRequest
import com.szymongrzybowicz.shoppinglist.shoppinglist.main.controller.create.CreateResponse
import com.szymongrzybowicz.shoppinglist.shoppinglist.main.controller.get.shopping.lists.GetShoppingListsResponse
import com.szymongrzybowicz.shoppinglist.shoppinglist.main.service.entity.ShoppingList
import com.szymongrzybowicz.shoppinglist.shoppinglist.main.service.mapper.ShoppingListMapper
import com.szymongrzybowicz.shoppinglist.shoppinglist.main.service.repository.ShoppingListRepository
import com.szymongrzybowicz.shoppinglist.user.main.service.repository.UserRepository
import com.szymongrzybowicz.shoppinglist.user.main.service.security.UserContext
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.sql.Timestamp

@Service
@Transactional
class ShoppingListService(
    private val repository: ShoppingListRepository,
    private val userContext: UserContext,
    private val shoppingListMapper: ShoppingListMapper,
    private val userRepository: UserRepository
) {

    fun getShoppingLists(page: Int?, onlyMyLists: Boolean?, onlySharedToMeLists: Boolean?): GetShoppingListsResponse {
        if (onlyMyLists == true && onlySharedToMeLists == true) {
            return GetShoppingListsResponse(
                true,
                listOf("Cannot search only my lists and only shared to me lists in one request"),
                ErrorCodes.BAD_REQUEST,
                null
            )
        }

        val lists =
            if (onlyMyLists == true) {
                repository.getShoppingListsByOwner(PageRequest.of(page ?: 0, 50), userContext.getCurrentUser().id)
            } else if (onlySharedToMeLists == true) {
                repository.getShoppingSharedToUser(PageRequest.of(page ?: 0, 50), userContext.getCurrentUser().id)
            } else {
                repository.getAllShoppingForUser(PageRequest.of(page ?: 0, 50), userContext.getCurrentUser().id)
            }

        val listsDto = lists.map { e -> shoppingListMapper.map(e) }
        return GetShoppingListsResponse(data = GetShoppingListsResponse.Data(listsDto))
    }

    fun createShoppingList(request: CreateRequest): CreateResponse {
        val userId = userContext.getCurrentUser().id
        val user = userRepository.findById(userId).get()
        var list =
            ShoppingList(
                owner = user,
                name = request.name,
                createDate = Timestamp(System.currentTimeMillis()),
                modificationDate = Timestamp(System.currentTimeMillis())
            )
        list = repository.save(list)
        return CreateResponse(data = CreateResponse.Data(list.id, list.name))
    }

    fun updateShoppingList(id: Long, name: String): EmptyResponse {
        val list = repository.findById(id).get()
        if (list.owner.id != userContext.getCurrentUser().id) {
            return EmptyResponse(
                true,
                listOf("User cannot edit list that he doesn't own"),
                errorCode = ErrorCodes.UNAUTHORIZED
            )
        }
        list.name = name
        list.modificationDate = Timestamp(System.currentTimeMillis())
        repository.save(list)
        return EmptyResponse()
    }

    fun delete(id: Long): EmptyResponse {
        val list = repository.findById(id).get()
        if (list.owner.id != userContext.getCurrentUser().id) {
            return EmptyResponse(
                true,
                listOf("User cannot delete list that he doesn't own"),
                errorCode = ErrorCodes.UNAUTHORIZED
            )
        }
        repository.deleteById(id)
        return EmptyResponse()
    }

}