package com.szymongrzybowicz.shoppinglist.user.main.service.mapper

import com.szymongrzybowicz.shoppinglist.user.main.dto.UserDto
import com.szymongrzybowicz.shoppinglist.user.main.service.entity.User
import org.springframework.stereotype.Component

@Component
class UserMapper {

    fun mapToDto(entity: User): UserDto {
        return UserDto(
            entity.firstName,
            entity.lastName,
            entity.mail,
            entity.role
        )
    }

    fun mapToDto(entity: List<User>): List<UserDto> {
        return entity.map { e -> mapToDto(e) }
    }
}