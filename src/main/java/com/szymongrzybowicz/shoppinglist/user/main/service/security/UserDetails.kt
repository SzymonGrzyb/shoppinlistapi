package com.szymongrzybowicz.shoppinglist.user.main.service.security

import com.szymongrzybowicz.shoppinglist.user.main.service.entity.User
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority


class UserDetails(private val user: User) : org.springframework.security.core.userdetails.UserDetails {

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return mutableListOf(SimpleGrantedAuthority(user.role))
    }

    val id get() = user.id

    override fun getPassword(): String {
        return user.password
    }

    override fun getUsername(): String {
        return user.mail
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun isEnabled(): Boolean {
        return true
    }
}