package com.szymongrzybowicz.shoppinglist.user.friend.service.entity

import com.szymongrzybowicz.shoppinglist.user.main.service.entity.User
import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(indexes = [Index(columnList = "user_from_id, user_to_id", unique = true)])
class Friend(

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    @Id
    val id: Long = -1,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_from_id")
    val userFrom: User,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_to_id")
    val userTo: User,

    @Column
    val createDate: Timestamp

)