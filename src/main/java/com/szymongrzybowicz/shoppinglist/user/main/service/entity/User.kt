package com.szymongrzybowicz.shoppinglist.user.main.service.entity

import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed
import javax.persistence.*

@Entity
@Indexed
@Table(name = "USERS", indexes = [Index(columnList = "mail")])
class User(

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    @Id
    val id: Long = -1,

    @Column
    val active: Boolean,

    @Column
    val role: String,

    @Column
    @FullTextField(analyzer = "userAnalyzer")
    val firstName: String,

    @Column
    @FullTextField(analyzer = "userAnalyzer")
    val lastName: String,

    @Column
    val password: String,

    @Column(unique = true)
    @FullTextField(analyzer = "userAnalyzer")
    val mail: String
)

