package com.szymongrzybowicz.shoppinglist.user.friend.controller.get.friends

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.szymongrzybowicz.shoppinglist.base.Response
import com.szymongrzybowicz.shoppinglist.user.main.dto.UserDto

@JsonIgnoreProperties(ignoreUnknown = true)
class GetFriendsResponse(
    hasErrors: Boolean,
    message: List<String>,
    errorCode: Int?,

    @JsonProperty
    val data: Data?

) : Response(hasErrors, message, errorCode) {

    class Data(
        @JsonProperty
        val friends: List<UserDto>
    )

}