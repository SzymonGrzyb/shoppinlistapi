package com.szymongrzybowicz.shoppinglist.shoppinglist.item.service.repository

import com.szymongrzybowicz.shoppinglist.shoppinglist.item.service.entity.ShoppingListItem
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ShoppingListItemRepository : CrudRepository<ShoppingListItem, Long> {

    fun getShoppingListItemsByShoppingListIdOrderById(page: Pageable, idShoppingList: Long): List<ShoppingListItem>
}