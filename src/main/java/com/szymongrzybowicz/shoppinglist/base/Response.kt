package com.szymongrzybowicz.shoppinglist.base

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
abstract class Response(

    @JsonProperty
    val hasErrors: Boolean,

    @JsonProperty
    var message: List<String>?,

    @JsonProperty
    var errorCode: Int?
)