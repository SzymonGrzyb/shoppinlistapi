package com.szymongrzybowicz.shoppinglist.user.friend.service.repository

import com.szymongrzybowicz.shoppinglist.user.friend.service.entity.Friend
import com.szymongrzybowicz.shoppinglist.user.main.service.entity.User
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
@Transactional
interface FriendRepository : JpaRepository<Friend, Long> {

    @Query(value = "SELECT u FROM Friend f JOIN f.userTo u WHERE f.userFrom.id = :userId")
    fun getFriends(pageable: Pageable, @Param("userId") currentUser: Long): Page<User>

    @Query(
        value = "SELECT i.id FROM FriendInvitation i " +
                "WHERE " +
                "(i.userFrom.id = :user1Id AND i.userTo.id = :user2Id)" +
                "OR" +
                "(i.userFrom.id = :user2Id AND i.userTo.id = :user1Id)"
    )
    fun getIdInvitation(@Param("user1Id") user1Id: Long, @Param("user2Id") user2Id: Long): Long?

    @Query(
        value = "DELETE FROM Friend f " +
                "WHERE " +
                "(f.userFrom.id = :user1Id AND f.userTo.id = :user2Id)" +
                "OR" +
                "(f.userFrom.id = :user2Id AND f.userTo.id = :user1Id)"
    )
    fun deleteFriend(@Param("user1Id") user1Id: Long, @Param("user2Id") user2Id: Long)
}