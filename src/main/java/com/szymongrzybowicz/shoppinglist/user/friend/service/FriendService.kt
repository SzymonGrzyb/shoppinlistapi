package com.szymongrzybowicz.shoppinglist.user.friend.service

import com.szymongrzybowicz.shoppinglist.base.EmptyResponse
import com.szymongrzybowicz.shoppinglist.base.ErrorCodes
import com.szymongrzybowicz.shoppinglist.user.friend.controller.get.friends.GetFriendsResponse
import com.szymongrzybowicz.shoppinglist.user.friend.controller.get.invitations.GetInvitationsResponse
import com.szymongrzybowicz.shoppinglist.user.friend.service.entity.Friend
import com.szymongrzybowicz.shoppinglist.user.friend.service.entity.FriendInvitation
import com.szymongrzybowicz.shoppinglist.user.friend.service.repository.FriendRepository
import com.szymongrzybowicz.shoppinglist.user.friend.service.repository.InvitationRepository
import com.szymongrzybowicz.shoppinglist.user.main.service.mapper.UserMapper
import com.szymongrzybowicz.shoppinglist.user.main.service.repository.UserRepository
import com.szymongrzybowicz.shoppinglist.user.main.service.security.UserContext
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.sql.Timestamp
import javax.persistence.EntityManager

@Service
@Transactional
class FriendService(
    private val friendRepository: FriendRepository,
    private val invitationRepository: InvitationRepository,
    private val userMapper: UserMapper,
    private val userContext: UserContext,
    private val userRepository: UserRepository,
    private val entityManager: EntityManager
) {

    fun getFriends(
        pageNumber: Int?,
        userId: Long?
    ): GetFriendsResponse {
        val page = PageRequest.of(pageNumber ?: 0, 100)
        val idUser = userId ?: userContext.getCurrentUser().id
        val users = friendRepository.getFriends(page, idUser).map { user -> userMapper.mapToDto(user) }.toList()
        val data = GetFriendsResponse.Data(users)
        return GetFriendsResponse(false, listOf(), null, data)
    }

    fun getInvitations(idUserTo: Long?, idUserFrom: Long?, pageNumber: Int?): GetInvitationsResponse {
        if (idUserFrom == null && idUserTo == null) {
            return GetInvitationsResponse(
                true,
                listOf("One of idUserFrom and idUserTo should be set."),
                ErrorCodes.FIELD_NOT_SET,
                null
            )
        }
        val page = PageRequest.of(pageNumber ?: 0, 100)
        val invitations: List<GetInvitationsResponse.Invitation> = idUserTo?.let {
            invitationRepository.findAllByUserToId(it, page).toList()//
                .map { i -> GetInvitationsResponse.Invitation(i.id, userMapper.mapToDto(i.userFrom)) }
        } ?: invitationRepository.findAllByUserFromId(idUserFrom!!, page).toList()//
            .map { i -> GetInvitationsResponse.Invitation(i.id, userMapper.mapToDto(i.userTo)) }

        return GetInvitationsResponse(false, listOf(), null, GetInvitationsResponse.Data(invitations))
    }

    fun createInvitation(userId: Long): EmptyResponse {
        if (userRepository.isUserBlockedByUser(userId, userContext.getCurrentUser().id)) {
            return EmptyResponse(
                true,
                listOf("User is Blocked by user that wants to invite"),
                ErrorCodes.USER_IS_BLOCKED
            )
        }

        if (friendRepository.getIdInvitation(userContext.getCurrentUser().id, userId) != null) {
            return EmptyResponse(
                true,
                listOf("Friend invitation for that users already exists"),
                ErrorCodes.INVITATION_ALREADY_EXISTS
            )
        }

        val invitation = FriendInvitation(
            userFrom = userRepository.findById(userContext.getCurrentUser().id).get(),
            userTo = userRepository.findById(userId).get(),
            createDate = Timestamp(System.currentTimeMillis())
        )

        entityManager.persist(invitation)

        //todo push notification
        return EmptyResponse()
    }

    fun respondInvitation(invitationId: Int, accept: Boolean): EmptyResponse {
        val friendInvitation = entityManager.find(FriendInvitation::class.java, invitationId)
        if (userRepository.isUserBlockedByUser(friendInvitation.id, userContext.getCurrentUser().id)) {
            return EmptyResponse(
                true,
                listOf("User is Blocked by user from invitation."),
                ErrorCodes.USER_IS_BLOCKED
            )
        }

        if (accept) {
            var friend =
                Friend(
                    userFrom = friendInvitation.userFrom,
                    userTo = friendInvitation.userTo,
                    createDate = Timestamp(System.currentTimeMillis())
                )
            entityManager.persist(friend)

            friend =
                Friend(
                    userFrom = friendInvitation.userTo,
                    userTo = friendInvitation.userFrom,
                    createDate = Timestamp(System.currentTimeMillis())
                )
            entityManager.persist(friend)
            //todo send push before delete to userFrom
        } else {
            //todo send push before delete to userFrom
        }
        entityManager.remove(friendInvitation)

        return EmptyResponse(false, null, null)
    }

    fun deleteFriend(userId: Long): EmptyResponse {
        val currentUserId = userContext.getCurrentUser().id
        friendRepository.deleteFriend(userId, currentUserId)
        return EmptyResponse()
    }

}