package com.szymongrzybowicz.shoppinglist.user.friend.service.repository

import com.szymongrzybowicz.shoppinglist.user.friend.service.entity.FriendInvitation
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
@Transactional
interface InvitationRepository : CrudRepository<FriendInvitation, Long> {

    fun findAllByUserToId(userToId: Long, pageable: Pageable): Page<FriendInvitation>
    fun findAllByUserFromId(userFromId: Long, pageable: Pageable): Page<FriendInvitation>

}