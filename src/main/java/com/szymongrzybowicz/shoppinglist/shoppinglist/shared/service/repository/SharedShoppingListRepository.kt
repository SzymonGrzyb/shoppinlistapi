package com.szymongrzybowicz.shoppinglist.shoppinglist.shared.service.repository

import com.szymongrzybowicz.shoppinglist.shoppinglist.shared.service.entity.SharedShoppingList
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface SharedShoppingListRepository : CrudRepository<SharedShoppingList, Long> {

    fun getByShoppingListIdAndUserId(shoppingListId: Long, userId: Long): SharedShoppingList?
}