package com.szymongrzybowicz.shoppinglist.user.main.service.security

import com.google.gson.Gson
import com.szymongrzybowicz.shoppinglist.base.ErrorCodes.LOGIN_OR_PASSWORD_INCORRECT
import com.szymongrzybowicz.shoppinglist.user.main.controller.login.LoginResponse
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.authentication.AuthenticationFailureHandler
import org.springframework.stereotype.Component
import java.io.IOException
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class LoginFailureHandler : AuthenticationFailureHandler {

    @Throws(IOException::class, ServletException::class)
    override fun onAuthenticationFailure(
        request: HttpServletRequest,
        response: HttpServletResponse,
        exception: AuthenticationException
    ) {
        val message = listOf("Email or password incorrect")
        val loginResponse = LoginResponse(true, message, LOGIN_OR_PASSWORD_INCORRECT, null)
        val json = Gson().toJson(loginResponse)
        val writer = response.writer
        writer.write(json)
        writer.flush()
        writer.close()
    }
}