package com.szymongrzybowicz.shoppinglist.shoppinglist.item.service.entity

import com.szymongrzybowicz.shoppinglist.shoppinglist.main.service.entity.ShoppingList
import java.math.BigDecimal
import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "shopping_list_item")
class ShoppingListItem(

    @Id
    @Column
    @GeneratedValue
    val id: Long = -1,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    val shoppingList: ShoppingList,

    @Column
    var name: String,

    @Column
    var quantity: BigDecimal,

    @Column
    var unit: String,

    @Column
    var bought: Boolean,

    @Column
    var createDate: Timestamp,

    @Column
    var modificationDate: Timestamp

)