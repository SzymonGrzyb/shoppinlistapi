package com.szymongrzybowicz.shoppinglist.shoppinglist.main.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.szymongrzybowicz.shoppinglist.user.main.dto.UserDto
import java.sql.Timestamp

@JsonIgnoreProperties(ignoreUnknown = true)
class ShoppingListDto(
    @JsonProperty
    val id: Long,
    @JsonProperty
    val owner: UserDto?,
    @JsonProperty
    val name: String,
    @JsonProperty
    val createDate: Timestamp,
    @JsonProperty
    val modificationDate: Timestamp,
    @JsonProperty
    val boughtItemsCount: Int,
    @JsonProperty
    val notBoughtItemsCount: Int
)
