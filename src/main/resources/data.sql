insert into shopping_list_permission(id, name)
values (1, 'READ') ON CONFLICT DO NOTHING;

insert into shopping_list_permission(id, name)
values (2, 'MARK_AS_BOUGHT') ON CONFLICT DO NOTHING;

insert into shopping_list_permission(id, name)
values (3, 'WRITE') ON CONFLICT DO NOTHING;