package com.szymongrzybowicz.shoppinglist.shoppinglist.main.service.entity

import com.szymongrzybowicz.shoppinglist.user.main.service.entity.User
import org.hibernate.annotations.Formula
import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "shopping_list", indexes = [Index(columnList = "create_date"), Index(columnList = "modification_date")])
class ShoppingList(

    @Id
    @Column
    @GeneratedValue
    val id: Long = -1,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    val owner: User,

    @Column
    var name: String,

    @Column(name = "create_date")
    val createDate: Timestamp,

    @Column(name = "modification_date")
    var modificationDate: Timestamp
) {

    @Formula(
        "(SELECT COUNT(si.id) FROM shopping_list_item si " +
                "WHERE si.shopping_list_id = id " +
                "AND si.bought = 'true')"
    )
    val boughtItemsCount: Int = 0

    @Formula(
        "(SELECT COUNT(si.id) FROM shopping_list_item si " +
                "WHERE si.shopping_list_id = id " +
                "AND si.bought = 'false')"
    )
    val notBoughtItemsCount: Int = 0
}