package com.szymongrzybowicz.shoppinglist.user.main.controller.login

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.szymongrzybowicz.shoppinglist.base.Response
import com.szymongrzybowicz.shoppinglist.user.main.dto.UserDto

@JsonIgnoreProperties(ignoreUnknown = true)
class LoginResponse(
    hasErrors: Boolean,
    message: List<String>?,
    errorCode: Int?,


    @JsonProperty
    val data: Data?

) : Response(hasErrors, message, errorCode) {
    class Data(
        val user: UserDto,
        val token: String
    )
}