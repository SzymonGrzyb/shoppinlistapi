package com.szymongrzybowicz.shoppinglist.shoppinglist.main.controller

import com.szymongrzybowicz.shoppinglist.base.EmptyResponse
import com.szymongrzybowicz.shoppinglist.shoppinglist.main.controller.create.CreateRequest
import com.szymongrzybowicz.shoppinglist.shoppinglist.main.controller.create.CreateResponse
import com.szymongrzybowicz.shoppinglist.shoppinglist.main.controller.get.shopping.lists.GetShoppingListsResponse
import com.szymongrzybowicz.shoppinglist.shoppinglist.main.service.ShoppingListService
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(
    "/shoppingList",
    produces = [MediaType.APPLICATION_JSON_VALUE],
    consumes = [MediaType.APPLICATION_JSON_VALUE]
)
@CrossOrigin("*")
class ShoppingListController(private val service: ShoppingListService) {

    @GetMapping
    fun getShoppingLists(
        @RequestParam(required = false) page: Int?,
        @RequestParam(required = false) onlyMyLists: Boolean?,
        @RequestParam(required = false) onlySharedToMeLists: Boolean?,
    ): GetShoppingListsResponse {
        return service.getShoppingLists(page, onlyMyLists, onlySharedToMeLists)
    }

    @PostMapping("/create")
    fun createShoppingList(@RequestBody request: CreateRequest): CreateResponse {
        return service.createShoppingList(request)
    }

    @PatchMapping("/update")
    fun updateShoppingList(
        @RequestParam(required = true) id: Long,
        @RequestParam(required = true) name: String
    ): EmptyResponse {
        return service.updateShoppingList(id, name)
    }

    @DeleteMapping
    fun deleteShoppingList(
        @RequestParam(required = true) id: Long,
    ): EmptyResponse {
        return service.delete(id)
    }

}