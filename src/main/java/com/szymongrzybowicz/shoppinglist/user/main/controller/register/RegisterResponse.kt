package com.szymongrzybowicz.shoppinglist.user.main.controller.register

import com.szymongrzybowicz.shoppinglist.base.Response

class RegisterResponse(
    hasErrors: Boolean = false,
    message: List<String>? = null,
    errorCode: Int? = null
) : Response(hasErrors, message, errorCode)