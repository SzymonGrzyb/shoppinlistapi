package com.szymongrzybowicz.shoppinglist

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
open class ShoppingListApplication

    fun main(args: Array<String>) {
        SpringApplication.run(ShoppingListApplication::class.java, *args)
    }
