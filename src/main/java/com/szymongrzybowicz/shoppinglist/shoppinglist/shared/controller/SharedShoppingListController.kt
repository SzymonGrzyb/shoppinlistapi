package com.szymongrzybowicz.shoppinglist.shoppinglist.shared.controller

import com.szymongrzybowicz.shoppinglist.base.EmptyResponse
import com.szymongrzybowicz.shoppinglist.shoppinglist.main.dto.Permission
import com.szymongrzybowicz.shoppinglist.shoppinglist.shared.service.SharedShoppingListService
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(
    "/shoppingList/share",
    produces = [MediaType.APPLICATION_JSON_VALUE],
    consumes = [MediaType.APPLICATION_JSON_VALUE]
)
@CrossOrigin("*")
class SharedShoppingListController(val service: SharedShoppingListService) {

    @PostMapping
    fun share(
        @RequestParam(required = true) idShoppingList: Long,
        @RequestParam(required = true) idUser: Long,
        @RequestParam(required = true) permission: Permission
    ): EmptyResponse {
        return service.share(idShoppingList, idUser, permission.id.toLong())
    }

    @DeleteMapping
    fun deleteSharing(@RequestParam(required = true) id: Long): EmptyResponse {
        return service.delete(id)
    }

    @PatchMapping("/updatePermission")
    fun updatePermission(
        @RequestParam(required = true) id: Long,
        @RequestParam(required = true) permission: Permission
    ): EmptyResponse {
        return service.updatePermission(id, permission.id.toLong())
    }

}