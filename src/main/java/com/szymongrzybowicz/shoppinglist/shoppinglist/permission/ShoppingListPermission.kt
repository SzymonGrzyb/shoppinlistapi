package com.szymongrzybowicz.shoppinglist.shoppinglist.permission

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class ShoppingListPermission(

    @Id
    @Column
    @GeneratedValue
    val id: Long = -1,

    @Column
    val name: String

) {
    companion object {
        const val READ = 1L
        const val MARK_AS_BOUGHT = 2L
        const val WRITE = 3L
    }
}