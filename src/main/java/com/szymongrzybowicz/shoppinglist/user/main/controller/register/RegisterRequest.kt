package com.szymongrzybowicz.shoppinglist.user.main.controller.register

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
class RegisterRequest(

    @JsonProperty
    val firstName: String,

    @JsonProperty
    val lastName: String,

    @JsonProperty
    val password: String?,

    @JsonProperty
    val mail: String
)