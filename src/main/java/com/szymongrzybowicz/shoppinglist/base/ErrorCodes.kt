package com.szymongrzybowicz.shoppinglist.base

object ErrorCodes {
    const val USER_ALREADY_EXISTS = 1
    const val LOGIN_OR_PASSWORD_INCORRECT = 2
    const val FIELD_NOT_SET = 3
    const val USER_IS_BLOCKED = 4
    const val INVITATION_ALREADY_EXISTS = 5
    const val BAD_REQUEST = 6
    const val UNAUTHORIZED = 7
}