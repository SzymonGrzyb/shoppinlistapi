package com.szymongrzybowicz.shoppinglist.user.main.controller.search

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.szymongrzybowicz.shoppinglist.base.Response
import com.szymongrzybowicz.shoppinglist.user.main.dto.UserDto

class SearchResponse(
    hasErrors: Boolean = false,
    message: List<String>? = null,
    errorCode: Int? = null,

    @JsonProperty
    val data: Data

) : Response(hasErrors, message, errorCode) {

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class Data(

        @JsonProperty
        val users: List<UserDto>

    )

}