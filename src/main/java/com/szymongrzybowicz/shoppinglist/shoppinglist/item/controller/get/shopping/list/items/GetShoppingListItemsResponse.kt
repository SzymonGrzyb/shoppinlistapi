package com.szymongrzybowicz.shoppinglist.shoppinglist.item.controller.get.shopping.list.items

import com.szymongrzybowicz.shoppinglist.base.Response
import com.szymongrzybowicz.shoppinglist.shoppinglist.item.dto.ShoppingListItemDto

class GetShoppingListItemsResponse(
    hasErrors: Boolean = false,
    message: List<String>? = null,
    errorCode: Int? = null,
    val data: Data? = null

) : Response(
    hasErrors,
    message,
    errorCode
) {
    class Data(
        val items: List<ShoppingListItemDto>
    )
}