package com.szymongrzybowicz.shoppinglist.user.friend.controller

import com.szymongrzybowicz.shoppinglist.base.EmptyResponse
import com.szymongrzybowicz.shoppinglist.user.friend.controller.get.friends.GetFriendsResponse
import com.szymongrzybowicz.shoppinglist.user.friend.controller.get.invitations.GetInvitationsResponse
import com.szymongrzybowicz.shoppinglist.user.friend.service.FriendService
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/friend", produces = [MediaType.APPLICATION_JSON_VALUE], consumes = [MediaType.APPLICATION_JSON_VALUE])
@CrossOrigin("*")
class FriendController(private val service: FriendService) {

    @GetMapping(value = ["/get"])
    fun getFriends(
        @RequestParam(required = false) page: Int?,
        @RequestParam(required = false) userId: Long?
    ): GetFriendsResponse {
        return service.getFriends(page, userId)
    }

    @GetMapping(value = ["/invitation/get"])
    fun getInvitations(
        @RequestParam(required = false) idUserTo: Long?,
        @RequestParam(required = false) idUserFrom: Long?,
        @RequestParam(required = false) pageNumber: Int?
    ): GetInvitationsResponse {
        return service.getInvitations(idUserTo, idUserFrom, pageNumber)
    }

    @PostMapping(value = ["/invitation/create"])
    fun createInvitation(@RequestParam userId: Long): EmptyResponse {
        return service.createInvitation(userId)
    }

    @PostMapping(value = ["/invitation/respond"])
    fun respondInvitation(@RequestParam invitationId: Int, @RequestParam accept: Boolean): EmptyResponse {
        return service.respondInvitation(invitationId, accept)
    }

    @DeleteMapping
    fun deleteFriend(@RequestParam userId: Long): EmptyResponse {
        return service.deleteFriend(userId)
    }

}