package com.szymongrzybowicz.shoppinglist.configuration

import org.apache.lucene.analysis.core.LowerCaseFilterFactory
import org.apache.lucene.analysis.core.WhitespaceTokenizerFactory
import org.apache.lucene.analysis.ngram.EdgeNGramFilterFactory
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory
import org.hibernate.search.backend.lucene.analysis.LuceneAnalysisConfigurationContext
import org.hibernate.search.backend.lucene.analysis.LuceneAnalysisConfigurer


class LuceneConfiguration : LuceneAnalysisConfigurer {

    override fun configure(context: LuceneAnalysisConfigurationContext) {
        context.analyzer("userAnalyzer").custom()
            .tokenizer(WhitespaceTokenizerFactory::class.java)
            .tokenFilter(LowerCaseFilterFactory::class.java)
            .tokenFilter(SnowballPorterFilterFactory::class.java)
            .param("language", "English")
            .tokenFilter(EdgeNGramFilterFactory::class.java)
            .param("maxGramSize", "40")
            .param("minGramSize", "3")
    }
}