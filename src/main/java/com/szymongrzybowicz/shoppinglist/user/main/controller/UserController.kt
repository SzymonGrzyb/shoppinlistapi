package com.szymongrzybowicz.shoppinglist.user.main.controller

import com.szymongrzybowicz.shoppinglist.base.EmptyResponse
import com.szymongrzybowicz.shoppinglist.user.main.controller.register.RegisterRequest
import com.szymongrzybowicz.shoppinglist.user.main.controller.register.RegisterResponse
import com.szymongrzybowicz.shoppinglist.user.main.controller.search.SearchResponse
import com.szymongrzybowicz.shoppinglist.user.main.service.UserService
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/user", produces = [MediaType.APPLICATION_JSON_VALUE], consumes = [MediaType.APPLICATION_JSON_VALUE])
@CrossOrigin("*")
class UserController(private val service: UserService) {

    @PostMapping(value = ["/register"])
    fun registerUser(@RequestBody request: RegisterRequest): RegisterResponse {
        return service.createUser(request.firstName, request.lastName, request.password, request.mail)
    }

    @GetMapping(value = ["/search"])
    fun search(@RequestParam text: String): SearchResponse {
        return service.search(text)
    }

    @PostMapping(value = ["/block"])
    fun blockUser(
        @RequestParam userId: Long,
        @RequestParam block: Boolean
    ): EmptyResponse {
        return service.block(userId, block)
    }
}