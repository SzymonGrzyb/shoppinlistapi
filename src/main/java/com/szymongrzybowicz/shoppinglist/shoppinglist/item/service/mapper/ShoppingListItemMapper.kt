package com.szymongrzybowicz.shoppinglist.shoppinglist.item.service.mapper

import com.szymongrzybowicz.shoppinglist.shoppinglist.item.dto.ShoppingListItemDto
import com.szymongrzybowicz.shoppinglist.shoppinglist.item.service.entity.ShoppingListItem
import org.springframework.stereotype.Component

@Component
class ShoppingListItemMapper {
    fun map(shoppingListItem: ShoppingListItem): ShoppingListItemDto {
        return ShoppingListItemDto(
            shoppingListItem.id,
            shoppingListItem.name,
            shoppingListItem.quantity,
            shoppingListItem.unit,
            shoppingListItem.bought,
            shoppingListItem.createDate,
            shoppingListItem.modificationDate
        )
    }
}