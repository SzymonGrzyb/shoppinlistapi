package com.szymongrzybowicz.shoppinglist.user.main.service.security

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

@Service
class UserContext {
    fun getCurrentUser(): UserDetails {
        val principal = SecurityContextHolder.getContext().authentication
        return (SecurityContextHolder.getContext().authentication.principal as UserDetails)
    }
}