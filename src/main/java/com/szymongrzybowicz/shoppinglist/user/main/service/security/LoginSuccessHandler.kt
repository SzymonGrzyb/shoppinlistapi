package com.szymongrzybowicz.shoppinglist.user.main.service.security

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.google.gson.Gson
import com.szymongrzybowicz.shoppinglist.user.main.controller.login.LoginResponse
import com.szymongrzybowicz.shoppinglist.user.main.service.mapper.UserMapper
import com.szymongrzybowicz.shoppinglist.user.main.service.repository.UserRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.AuthenticationSuccessHandler
import org.springframework.stereotype.Component
import java.io.IOException
import java.util.*
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class LoginSuccessHandler(
    @Value("\${jwt.expirationTime}") private val expirationTime: Long,
    @Value("\${jwt.secret}") private val secret: String,
    private val userRepository: UserRepository,
    private val userMapper: UserMapper,
    private val userContext: UserContext
) : AuthenticationSuccessHandler {


    @Throws(IOException::class, ServletException::class)
    override fun onAuthenticationSuccess(
        request: HttpServletRequest,
        response: HttpServletResponse,
        authentication: Authentication
    ) {
        logger.info("request($request), response($response), authentication($authentication); onAuthenticationSuccess()")
        response.status = HttpServletResponse.SC_OK
        val user = userRepository.findByMail(userContext.getCurrentUser().username)!!
        val token = JWT.create()
            .withSubject(user.mail)
            .withExpiresAt(Date(System.currentTimeMillis() + expirationTime))
            .sign(Algorithm.HMAC256(secret))

        val data = LoginResponse.Data(userMapper.mapToDto(user), token)
        val loginResponse = LoginResponse(false, null, null, data)
        val json = Gson().toJson(loginResponse)
        val writer = response.writer
        writer.write(json)
        writer.flush()
        writer.close()
    }

    private val logger = LoggerFactory.getLogger(javaClass)
}