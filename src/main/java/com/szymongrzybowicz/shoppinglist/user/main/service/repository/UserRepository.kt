package com.szymongrzybowicz.shoppinglist.user.main.service.repository

import com.szymongrzybowicz.shoppinglist.user.main.service.entity.User
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
@Transactional
interface UserRepository : CrudRepository<User, Long> {
    override fun findAll(): List<User>
    fun findByMail(mail: String): User?

    @Query("SELECT CASE  WHEN count(bu.id) > 0 THEN true ELSE false END FROM BlockedUser bu WHERE bu.userFrom.id = :userFromId AND bu.userTo.id = :userToId")
    fun isUserBlockedByUser(@Param("userToId") userToId: Long, @Param("userFromId") userFromId: Long): Boolean

    @Query("DELETE FROM BlockedUser bu WHERE bu.userFrom.id = :userFromId AND bu.userFrom.id = :userToId")
    fun removeBlockade(@Param("userFromId") userFromId: Long, @Param("userToId") userToId: Long)
}