package com.szymongrzybowicz.shoppinglist.user.main.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
class UserDto(

    @JsonProperty
    val firstName: String,

    @JsonProperty
    val lastName: String,

    @JsonProperty
    val mail: String,

    @JsonProperty
    val role: String?
)