package com.szymongrzybowicz.shoppinglist.user.main.service.security

import org.slf4j.LoggerFactory
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler
import org.springframework.stereotype.Component
import java.io.IOException
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class LogoutSuccessHandler : LogoutSuccessHandler {

    @Throws(IOException::class, ServletException::class)
    override fun onLogoutSuccess(
        request: HttpServletRequest,
        response: HttpServletResponse,
        authentication: Authentication?
    ) {
        logger.info("request($request), response($response), authentication($authentication); onLogoutSuccess()")
        response.status = HttpServletResponse.SC_OK
    }

    private val logger = LoggerFactory.getLogger(javaClass)
}