package com.szymongrzybowicz.shoppinglist.shoppinglist.main.service.repository

import com.szymongrzybowicz.shoppinglist.shoppinglist.main.service.entity.ShoppingList
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface ShoppingListRepository : CrudRepository<ShoppingList, Long> {

    @Query("FROM ShoppingList l WHERE l.owner.id = :ownerId ORDER BY l.modificationDate")
    fun getShoppingListsByOwner(pageable: Pageable, @Param("ownerId") ownerId: Long): List<ShoppingList>

    @Query(
        "SELECT l FROM SharedShoppingList sl " +
                "JOIN sl.shoppingList l " +
                "WHERE sl.user.id = :userId ORDER BY l.modificationDate"
    )
    fun getShoppingSharedToUser(pageable: Pageable, @Param("userId") userId: Long): List<ShoppingList>

    @Query(
        "SELECT l FROM ShoppingList l " +
                "WHERE (l.owner.id = :userId) OR " +
                "l.id IN (SELECT sl.shoppingList.id FROM SharedShoppingList sl WHERE sl.user.id = :userId) " +
                "ORDER BY l.modificationDate"
    )
    fun getAllShoppingForUser(pageable: Pageable, @Param("userId") userId: Long): List<ShoppingList>
}