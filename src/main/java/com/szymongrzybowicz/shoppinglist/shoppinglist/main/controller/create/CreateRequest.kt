package com.szymongrzybowicz.shoppinglist.shoppinglist.main.controller.create

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class CreateRequest(
    @JsonProperty
    val name: String = ""
) {

    constructor() : this("")
}