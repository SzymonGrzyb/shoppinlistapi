package com.szymongrzybowicz.shoppinglist.user.friend.controller.get.invitations

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.szymongrzybowicz.shoppinglist.base.Response
import com.szymongrzybowicz.shoppinglist.user.main.dto.UserDto

@JsonIgnoreProperties(ignoreUnknown = true)
class GetInvitationsResponse(
    hasErrors: Boolean,
    message: List<String>,
    errorCode: Int?,

    @JsonProperty
    val data: Data?

) : Response(hasErrors, message, errorCode) {

    data class Data(
        @JsonProperty
        val invitations: List<Invitation>
    )

    data class Invitation(

        @JsonProperty
        val idInvitation: Long,

        @JsonProperty
        val user: UserDto
    )

}