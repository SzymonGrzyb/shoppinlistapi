package com.szymongrzybowicz.shoppinglist.user.main.service.security

import com.fasterxml.jackson.databind.ObjectMapper
import com.szymongrzybowicz.shoppinglist.user.main.controller.login.LoginRequest
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import java.io.IOException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class JsonObjectAuthenticationFilter : UsernamePasswordAuthenticationFilter() {
    private val objectMapper = ObjectMapper()

    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication {
        return try {
            val reader = request.reader
            val sb = StringBuilder()
            var line: String?
            while (reader.readLine().also { line = it } != null) {
                sb.append(line)
            }
            val authRequest = objectMapper.readValue(sb.toString(), LoginRequest::class.java)
            val token = UsernamePasswordAuthenticationToken(
                authRequest.mail, authRequest.password
            )
            setDetails(request, token)
            authenticationManager.authenticate(token)
        } catch (e: IOException) {
            throw IllegalArgumentException(e.message)
        }
    }
}