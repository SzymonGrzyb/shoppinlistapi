package com.szymongrzybowicz.shoppinglist.shoppinglist.item.service

import com.szymongrzybowicz.shoppinglist.base.EmptyResponse
import com.szymongrzybowicz.shoppinglist.base.ErrorCodes
import com.szymongrzybowicz.shoppinglist.shoppinglist.item.controller.create.CreateRequest
import com.szymongrzybowicz.shoppinglist.shoppinglist.item.controller.create.CreateResponse
import com.szymongrzybowicz.shoppinglist.shoppinglist.item.controller.get.shopping.list.items.GetShoppingListItemsResponse
import com.szymongrzybowicz.shoppinglist.shoppinglist.item.service.entity.ShoppingListItem
import com.szymongrzybowicz.shoppinglist.shoppinglist.item.service.mapper.ShoppingListItemMapper
import com.szymongrzybowicz.shoppinglist.shoppinglist.item.service.repository.ShoppingListItemRepository
import com.szymongrzybowicz.shoppinglist.shoppinglist.main.service.repository.ShoppingListRepository
import com.szymongrzybowicz.shoppinglist.shoppinglist.shared.service.SharedShoppingListService
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import java.sql.Timestamp

@Service
@Transactional
class ShoppingListItemService(
    private val repository: ShoppingListItemRepository,
    private val mapper: ShoppingListItemMapper,
    private val shoppingListRepository: ShoppingListRepository,
    private val sharedShoppingListService: SharedShoppingListService
) {
    fun getItems(page: Int?, idShoppingList: Long): GetShoppingListItemsResponse {
        if (!sharedShoppingListService.checkUserHasReadPermission(idShoppingList)) {
            return GetShoppingListItemsResponse(
                true,
                listOf("User has no read permission to this list"),
                ErrorCodes.UNAUTHORIZED
            )
        }
        val items =
            repository.getShoppingListItemsByShoppingListIdOrderById(PageRequest.of(page ?: 0, 50), idShoppingList)
                .map { e -> mapper.map(e) }

        return GetShoppingListItemsResponse(data = GetShoppingListItemsResponse.Data(items))
    }

    fun create(request: CreateRequest): CreateResponse {
        val shoppingList = shoppingListRepository.findById(request.idShoppingList).get()
        if (!sharedShoppingListService.checkUserHasWritePermission(shoppingList.id)) {
            return CreateResponse(
                true,
                listOf("User has no permission to add list item"),
                ErrorCodes.UNAUTHORIZED
            )
        }
        var item = ShoppingListItem(
            shoppingList = shoppingList,
            name = request.name,
            quantity = request.quantity,
            unit = request.unit,
            createDate = Timestamp(System.currentTimeMillis()),
            modificationDate = Timestamp(System.currentTimeMillis()),
            bought = false
        )
        item = repository.save(item)

        val list = item.shoppingList
        list.modificationDate = Timestamp(System.currentTimeMillis())
        shoppingListRepository.save(list)

        return CreateResponse(data = CreateResponse.Data(item.id, item.name))
    }

    fun updateItem(id: Long, name: String?, quantity: BigDecimal?, unit: String?, bought: Boolean?): EmptyResponse {
        if (name == null && quantity == null && unit == null) {
            return EmptyResponse(
                true,
                message = listOf("At least one parameter is required for update"),
                errorCode = ErrorCodes.BAD_REQUEST
            )
        }
        val item = repository.findById(id).get()
        if (name != null || quantity != null || unit != null) {
            if (!sharedShoppingListService.checkUserHasWritePermission(item.shoppingList.id)) {
                return EmptyResponse(
                    true,
                    listOf("User has no permission to edit list item"),
                    ErrorCodes.UNAUTHORIZED
                )
            } else if (!sharedShoppingListService.checkUserHasMarkAsBoughtPermission(item.shoppingList.id)) {
                return EmptyResponse(
                    true,
                    listOf("User has no permission to mark list item as bought"),
                    ErrorCodes.UNAUTHORIZED
                )
            }
        }

        if (name != null) {
            item.name = name
        }

        if (quantity != null) {
            item.quantity = quantity
        }

        if (unit != null) {
            item.unit = unit
        }

        if (bought != null) {
            item.bought = bought
        }

        item.modificationDate = Timestamp(System.currentTimeMillis())
        repository.save(item)

        val list = item.shoppingList
        list.modificationDate = Timestamp(System.currentTimeMillis())
        shoppingListRepository.save(list)

        return EmptyResponse()
    }

    fun delete(id: Long): EmptyResponse {
        val item = repository.findById(id).get()

        if (!sharedShoppingListService.checkUserHasWritePermission(item.shoppingList.id)) {
            return EmptyResponse(
                true,
                listOf("User has no permission to delete list item"),
                ErrorCodes.UNAUTHORIZED
            )
        }

        val list = item.shoppingList
        list.modificationDate = Timestamp(System.currentTimeMillis())
        shoppingListRepository.save(list)

        repository.delete(item)
        return EmptyResponse()
    }


}