package com.szymongrzybowicz.shoppinglist.user.main.controller.login

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
class LoginRequest(

    @JsonProperty(value = "password")
    val password: String,

    @JsonProperty(value = "mail")
    val mail: String
)