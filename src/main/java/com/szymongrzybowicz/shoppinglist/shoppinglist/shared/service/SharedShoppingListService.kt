package com.szymongrzybowicz.shoppinglist.shoppinglist.shared.service

import com.szymongrzybowicz.shoppinglist.base.EmptyResponse
import com.szymongrzybowicz.shoppinglist.base.ErrorCodes
import com.szymongrzybowicz.shoppinglist.shoppinglist.main.dto.Permission
import com.szymongrzybowicz.shoppinglist.shoppinglist.main.service.entity.ShoppingList
import com.szymongrzybowicz.shoppinglist.shoppinglist.main.service.repository.ShoppingListRepository
import com.szymongrzybowicz.shoppinglist.shoppinglist.permission.ShoppingListPermission
import com.szymongrzybowicz.shoppinglist.shoppinglist.permission.ShoppingListPermissionRepository
import com.szymongrzybowicz.shoppinglist.shoppinglist.shared.service.entity.SharedShoppingList
import com.szymongrzybowicz.shoppinglist.shoppinglist.shared.service.repository.SharedShoppingListRepository
import com.szymongrzybowicz.shoppinglist.user.main.service.repository.UserRepository
import com.szymongrzybowicz.shoppinglist.user.main.service.security.UserContext
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.sql.Timestamp

@Service
@Transactional
class SharedShoppingListService(
    private val repository: SharedShoppingListRepository,
    private val shoppingListRepository: ShoppingListRepository,
    private val permissionRepository: ShoppingListPermissionRepository,
    private val userRepository: UserRepository,
    private val userContext: UserContext
) {
    fun share(idShoppingList: Long, idUser: Long, idPermission: Long): EmptyResponse {
        if (idPermission == Permission.OWNER.id.toLong()) {
            return EmptyResponse(true, listOf("Cannot share as owner"), ErrorCodes.BAD_REQUEST)
        }

        val permission = permissionRepository.findById(idPermission).get()
        val user = userRepository.findById(userContext.getCurrentUser().id).get()

        var sharedShoppingList = repository.getByShoppingListIdAndUserId(idShoppingList, idUser)
        if (sharedShoppingList != null) {
            return updatePermission(sharedShoppingList.id, idPermission)
        }

        val shoppingList = shoppingListRepository.findById(idShoppingList).get()

        sharedShoppingList = SharedShoppingList(
            shoppingList = shoppingList,
            user = user,
            shoppingListPermission = permission,
            createDate = Timestamp(System.currentTimeMillis()),
            modificationDate = Timestamp(System.currentTimeMillis())
        )

        repository.save(sharedShoppingList)
        return EmptyResponse()
    }

    fun delete(id: Long): EmptyResponse {
        val userId = userContext.getCurrentUser().id
        val sharedShoppingList = repository.findById(id).get()
        if (sharedShoppingList.user.id == userId || sharedShoppingList.shoppingList.owner.id == userId) {
            repository.delete(sharedShoppingList)
            return EmptyResponse()
        }
        return EmptyResponse(
            true,
            listOf("Only owner of shopping list or user that sharing refers to can delete sharing"),
            ErrorCodes.UNAUTHORIZED
        )
    }

    fun updatePermission(id: Long, idPermission: Long): EmptyResponse {
        if (idPermission == Permission.OWNER.id.toLong()) {
            return EmptyResponse(true, listOf("Cannot share as owner"), ErrorCodes.BAD_REQUEST)
        }

        val permission = permissionRepository.findById(idPermission).get()
        val user = userRepository.findById(userContext.getCurrentUser().id).get()

        var sharedShoppingList = repository.findById(id).get()
        if (sharedShoppingList.shoppingList.owner.id != user.id) {
            return EmptyResponse(
                true,
                listOf("Only owner can change shopping list permission"),
                ErrorCodes.UNAUTHORIZED
            )
        }
        sharedShoppingList.shoppingListPermission = permission
        repository.save(sharedShoppingList)
        return EmptyResponse()
    }

    fun checkUserHasWritePermission(idShoppingList: Long): Boolean {
        val shoppingList = shoppingListRepository.findById(idShoppingList).get()
        if (isCurrentUserListOwner(shoppingList)) {
            return true
        }

        val idShoppingListPermission =
            permissionRepository.getIdShoppingListPermissionByIdUserAndIdShoppingList(
                userContext.getCurrentUser().id,
                shoppingList.id
            )
        if (idShoppingListPermission != ShoppingListPermission.WRITE) {
            return false
        }

        return true
    }

    fun checkUserHasMarkAsBoughtPermission(idShoppingList: Long): Boolean {
        val shoppingList = shoppingListRepository.findById(idShoppingList).get()
        if (isCurrentUserListOwner(shoppingList)) {
            return true
        }

        val idShoppingListPermission =
            permissionRepository.getIdShoppingListPermissionByIdUserAndIdShoppingList(
                userContext.getCurrentUser().id,
                shoppingList.id
            )
        if (idShoppingListPermission != ShoppingListPermission.MARK_AS_BOUGHT || idShoppingListPermission != ShoppingListPermission.WRITE) {
            return false
        }

        return true
    }

    fun checkUserHasReadPermission(idShoppingList: Long): Boolean {
        val shoppingList = shoppingListRepository.findById(idShoppingList).get()
        if (isCurrentUserListOwner(shoppingList)) {
            return true
        }

        val idShoppingListPermission =
            permissionRepository.getIdShoppingListPermissionByIdUserAndIdShoppingList(
                userContext.getCurrentUser().id,
                shoppingList.id
            )
        if (idShoppingListPermission != ShoppingListPermission.MARK_AS_BOUGHT || idShoppingListPermission != ShoppingListPermission.WRITE || idShoppingListPermission != ShoppingListPermission.READ) {
            return false
        }

        return true
    }

    private fun isCurrentUserListOwner(shoppingList: ShoppingList): Boolean {
        return shoppingList.owner.id == userContext.getCurrentUser().id
    }

}