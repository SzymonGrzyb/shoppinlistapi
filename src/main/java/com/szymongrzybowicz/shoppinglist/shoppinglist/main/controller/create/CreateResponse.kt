package com.szymongrzybowicz.shoppinglist.shoppinglist.main.controller.create

import com.szymongrzybowicz.shoppinglist.base.Response

class CreateResponse(
    hasErrors: Boolean = false,
    message: List<String>? = null,
    errorCode: Int? = null,
    val data: Data
) : Response(hasErrors, message, errorCode) {

    class Data(
        val id: Long,
        val name: String
    )
}