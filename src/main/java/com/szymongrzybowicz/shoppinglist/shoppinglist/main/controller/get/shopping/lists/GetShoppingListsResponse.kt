package com.szymongrzybowicz.shoppinglist.shoppinglist.main.controller.get.shopping.lists

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.szymongrzybowicz.shoppinglist.base.Response
import com.szymongrzybowicz.shoppinglist.shoppinglist.main.dto.ShoppingListDto

@JsonIgnoreProperties(ignoreUnknown = true)
class GetShoppingListsResponse(
    hasErrors: Boolean = false,
    message: List<String>? = null,
    errorCode: Int? = null,
    var data: Data?
) : Response(
    hasErrors, message, errorCode
) {

    class Data(
        val shoppingLists: List<ShoppingListDto>
    )
}