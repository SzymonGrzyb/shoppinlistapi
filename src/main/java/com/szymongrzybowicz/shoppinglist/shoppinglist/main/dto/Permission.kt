package com.szymongrzybowicz.shoppinglist.shoppinglist.main.dto

enum class Permission(val id: Int) {
    READ(1), MARK_AS_BOUGHT(2), WRITE(3), OWNER(4)
}