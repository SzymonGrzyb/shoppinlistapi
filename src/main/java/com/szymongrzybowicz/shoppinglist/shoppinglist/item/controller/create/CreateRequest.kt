package com.szymongrzybowicz.shoppinglist.shoppinglist.item.controller.create

import java.math.BigDecimal

class CreateRequest(
    val idShoppingList: Long,
    val name: String,
    val quantity: BigDecimal,
    val unit: String
)