package com.szymongrzybowicz.shoppinglist.shoppinglist.permission

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface ShoppingListPermissionRepository : CrudRepository<ShoppingListPermission, Long> {

    @Query(
        "SELECT p.id FROM SharedShoppingList sh " +
                "JOIN sh.shoppingListPermission p " +
                "WHERE sh.user.id = :idUser AND sh.shoppingList.id = :idShoppingList"
    )
    fun getIdShoppingListPermissionByIdUserAndIdShoppingList(
        @Param("idUser") idUser: Long,
        @Param("idShoppingList") idShoppingList: Long
    ): Long?
}