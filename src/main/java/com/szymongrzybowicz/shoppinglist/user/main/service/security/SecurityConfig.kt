package com.szymongrzybowicz.shoppinglist.user.main.service.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import java.util.*


@Configuration
@EnableWebSecurity
open class SecurityConfig(
    @Value("\${jwt.secret}") private val secret: String,
    private val myUserDetailsService: MyUserDetailsService,
    private val loginFailureHandler: LoginFailureHandler,
    private val loginSuccessHandler: LoginSuccessHandler,
    private val logoutSuccessHandler: LogoutSuccessHandler
) : WebSecurityConfigurerAdapter() {

    @Autowired
    fun configureGlobal(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService(myUserDetailsService).passwordEncoder(passwordEncoder())
    }

    @Bean
    open fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean
    open fun corsConfigurationSource(): CorsConfigurationSource {
        val configuration = CorsConfiguration()
        configuration.allowedOrigins = Arrays.asList("*")
        configuration.allowedMethods = Arrays.asList("GET", "POST", "DELETE", "PATCH", "PUT")
        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", configuration)
        return source
    }

    @Bean
    @Throws(Exception::class)
    open fun authenticationFilter(): JsonObjectAuthenticationFilter {
        val filter = JsonObjectAuthenticationFilter()
        filter.setAuthenticationSuccessHandler(loginSuccessHandler)
        filter.setAuthenticationFailureHandler(loginFailureHandler)
        filter.setAuthenticationManager(super.authenticationManagerBean())
        return filter
    }

    override fun configure(httpSecurity: HttpSecurity) {
        httpSecurity
            .exceptionHandling()
            .authenticationEntryPoint(Http403ForbiddenEntryPoint())
            .and()
            .authorizeRequests().antMatchers("/user/register").permitAll()
            .and()
            .authorizeRequests().antMatchers("/friend/**", "/user/**", "/shoppingList/**")
            .access("hasRole('" + UserRole.ROLE_USER + "') or hasRole('" + UserRole.ROLE_USER + "')")
            .and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .addFilter(authenticationFilter())
            .addFilter(JwtAuthorizationFilter(authenticationManager(), super.userDetailsService(), secret))
            .exceptionHandling()
            .and()
            .csrf().disable()
            .logout()
            .logoutRequestMatcher(AntPathRequestMatcher("/logout"))
            .logoutSuccessHandler(logoutSuccessHandler)
    }
}